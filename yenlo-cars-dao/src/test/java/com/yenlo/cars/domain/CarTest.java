package com.yenlo.cars.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class CarTest {
  private Car car = new Car("1", "Mark", "Model", "Type", new Date(), 2000, 5, 1998, 1569, 8);

  @Test
  public void test() {
    assertNotNull(car);
    assertNotNull(car.getId());
    assertTrue(0 == car.getId());
    assertNotNull(car.getMark());
    assertEquals("Mark", car.getMark());
    assertNotNull(car.getType());
    assertEquals("Type", car.getType());
    assertNotNull(car.getModel());
    assertEquals("Model", car.getModel());
    assertNotNull(car.getRegistered());
    assertNull(car.getUnregistered());
    car.setUnregistered(car.getRegistered());
    assertEquals(car.getRegistered(),car.getUnregistered());
    assertNotNull(car.getBPM());
    assertNotNull(car.getCapacity());
    assertNull(car.getNotes());
    car.setNotes("Notes");
    assertNotNull(car.getNotes());
  }

}

package com.yenlo.cars.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.yenlo.cars.dao.Cars;
import com.yenlo.cars.domain.Car;

// INSERT not working on database placed on Heroku
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/yenlo-cars-dao-context.xml"})
@Transactional
@Rollback
public class CarsRepositoryTest {
  @Autowired
  private Cars cars;
  
  private Car car = new Car("1", "Mark", "Type", "Name", new Date(), 2000, 5, 1998, 1569, 8);
  
  @Test
  public void testFind() {
    assertNotNull(cars);
    assertNotNull(car);
//    Car item = cars.save(car);
//    assertNotNull(item);
//    List<Car> list = cars.find();
//    assertNotNull(list);
//    assertFalse(list.isEmpty());
//    assertTrue(list.size() > 0);
//    
//    assertTrue(list.contains(car));
//    assertTrue(list.indexOf(car) > -1);
//    
//    int index = list.indexOf(car);    
//    assertEquals(list.get(index), car);
//    assertEquals(list.get(index).getVehicleRegistrationPlate(), car.getVehicleRegistrationPlate());
  }

//  @Test
  public void testFindById() {
    assertNotNull(cars);
    assertNotNull(car);
//    Car item = cars.save(car);
//    assertNotNull(item);
//    Car found = cars.find(car.getVehicleRegistrationPlate());
//    assertNotNull(found);
// 
//    assertEquals(found, car);
  }
}

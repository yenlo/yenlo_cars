package com.yenlo.cars.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Repository;

import com.yenlo.cars.dao.Cars;
import com.yenlo.cars.domain.Car;

/**
 * Car DAO
 * 
 * @author Thomas Zielinski, 2016
 */
@Repository
public class CarsRepository extends AbstractRepository<Car, Integer> implements Cars {
  private final static String COUNT = "select count(1) from Car c";
  private final static String COUNT_MARK_DISTICT = "select distinct(c.mark), count(1) from Car c group by c.mark";
  private final static String COUNT_MARK = COUNT + " where c.mark%s:mark";
  private final static String COUNT_MARK_MODEL = COUNT_MARK + " and c.model%s:model";
  private final static String COUNT_MARK_TYPE = COUNT_MARK + " and c.type%s:type";
  private final static String SELECT = "select c from Car c";
  private final static String SELECT_MARK = SELECT + " where c.mark%s:mark";
  
  public CarsRepository() {
    super(Car.class);
  }
  
  @Override
  public long count() {
    Object result = getEntityManager().createQuery(COUNT).getSingleResult();
    return result instanceof Long ? ((Long)result).longValue() : 0L;
  }
  
  @Override
  public Map<String, Long> countMark() {
    Map<String, Long> map = new TreeMap<>();
    List<?> result = getEntityManager().createQuery(COUNT_MARK_DISTICT).getResultList();
    for (Object obj : result) {
      if (obj.getClass().isArray() && ((Object[])obj).length >= 2) {
        if (((Object[])obj)[0] instanceof String && ((Object[])obj)[1] instanceof Number) {
          map.put((String)((Object[])obj)[0], ((Number)((Object[])obj)[1]).longValue());
        }
      }
    }
    return map;
  }
  
  @Override
  public long countMark(String mark) {
    Object result = mark == null ? null :
      getEntityManager()
        .createQuery(String.format(COUNT_MARK, operator(mark)))
        .setParameter("mark", mark.toUpperCase()).getSingleResult();
    return result instanceof Long ? ((Long)result).longValue() : 0L;
  }
  
  @Override
  public long countMarkModel(String mark, String model) {
    Object result = mark == null || model == null ? null :
        getEntityManager()
          .createQuery(String.format(COUNT_MARK_MODEL, operator(mark), operator(model)))
          .setParameter("mark", mark.toUpperCase())
          .setParameter("model", model).getSingleResult();
    return result instanceof Long ? ((Long)result).longValue() : 0L;
  }
  
  @Override
  public long countMarkType(String mark, String type) {
    Object result = mark == null || type == null ? null :
        getEntityManager()
          .createQuery(String.format(COUNT_MARK_TYPE, operator(mark), operator(type)))
          .setParameter("mark", mark.toUpperCase())
          .setParameter("type", type)
          .getSingleResult();
    return result instanceof Long ? ((Long)result).longValue() : 0L;
  }
  
  @Override
  public Car find(String vehicleRegistrationPlate) {    
    return getEntityManager()
        .createNamedQuery("byVRP", Car.class)
        .setParameter("vehicleRegistrationPlate", vehicleRegistrationPlate.toUpperCase())
        .getSingleResult();
  }
  
  @Override
  public List<Car> find() {
    return getEntityManager().createQuery("from Car", Car.class).getResultList();
  }

  @Override
  public List<Car> getMark(String mark) {
    return mark == null ? null :
      getEntityManager()
        .createQuery(String.format(SELECT_MARK, operator(mark)), Car.class)
        .setParameter("mark", mark.toUpperCase())
        .getResultList();
  }
  
  @Override
  public List<Car> getType(String type) {
    return type == null ? null :
      getEntityManager()
        .createQuery(String.format(SELECT + " where type%s:type", operator(type)), Car.class)
        .setParameter("type", type)
        .getResultList();
  }
  
  @Override
  public List<Car> getModel(String model) {
    return model == null ? null :
      getEntityManager()
        .createQuery(String.format(SELECT + " where model%s:model", operator(model)), Car.class)
        .setParameter("model", model)
        .getResultList();
  }
  
  @Override
  public List<Car> getMarkType(String mark, String type) {
    return mark == null || type == null ? null :
      getEntityManager()
        .createQuery(String.format(SELECT_MARK + " and type%s:type", operator(mark), operator(type)), Car.class)
        .setParameter("mark", mark.toUpperCase())
        .setParameter("type", type)
        .getResultList();
  }
  
  @Override
  public List<Car> getMarkModel(String mark, String model) {
    return mark == null || model == null ? null :
      getEntityManager()
        .createQuery(String.format(SELECT_MARK + " and model%s:model", operator(mark), operator(model)), Car.class)
        .setParameter("mark", mark.toUpperCase())
        .setParameter("model", model)
        .getResultList();
  }
  
  @Override
  public Car changeVehicleRegistrationPlate(String oldVehicleRegistrationPlate, String newVehicleRegistrationPlate) {
    Car car = find(oldVehicleRegistrationPlate);
    if (car != null && 
        newVehicleRegistrationPlate != null && 
        !newVehicleRegistrationPlate.trim().isEmpty() &&
        newVehicleRegistrationPlate.trim().equalsIgnoreCase(
            oldVehicleRegistrationPlate != null ? oldVehicleRegistrationPlate.trim() : oldVehicleRegistrationPlate)) {
      car.setVehicleRegistrationPlate(newVehicleRegistrationPlate);
      return save(car);
    }
    return car;
  }
  
  @Override
  public Car setNotes(String licensePlate, String notes) {
    Car car = find(licensePlate);
    if (car != null) {
      car.setNotes(notes);
      return save(car);
    }
    return car;
  }
  
  @Override
  public Car appendNotes(String licensePlate, String notes) {
    Car car = find(licensePlate);
    if (car != null) {
      car.setNotes(car.getNotes() + notes);
      return save(car);
    }
    return car;
  }

  @Override
  public Car unregister(String licensePlate, Date date) {
    Car car = find(licensePlate);
    if (car != null) {
      car.setUnregistered(date);
      return save(car);
    }
    return car;
  }
  
  private String operator(String text) {
    return text.trim().startsWith("%") || text.trim().endsWith("%") ? " like " : "=";
  }
}
package com.yenlo.cars.dao;

import org.springframework.transaction.annotation.Transactional;

public interface Writeable<T> {
  @Transactional
  T save(T entity);
}

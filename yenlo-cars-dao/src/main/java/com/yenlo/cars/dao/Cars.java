package com.yenlo.cars.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.transaction.annotation.Transactional;

import com.yenlo.cars.domain.Car;

/**
 * Car DAO
 * 
 * @author Thomas Zielinski, 2016
 */
public interface Cars extends Writeable<Car>, Findable<Car, Integer> {
  /**
   * Count a number of records in database
   * @return the number of records in database
   */
  @Transactional(readOnly = true)
  long count();
  
  /**
   * Get an information about the car with given vehicle registration plate (license plate)
   * @param vehicleRegistrationPlate the vehicle registration plate (license plate)
   * @return the information about the car
   */
  @Transactional(readOnly=true)
  Car find(String vehicleRegistrationPlate);
  
  /**
   * Get a list of information about the cars for given mark
   * @param mark the mark of the car
   * @return the list of the information about the cars
   */
  @Transactional(readOnly = true)
  List<Car> getMark(String mark);
  
  /**
   * Get a number of records in database for marks
   * @return the number of records in database for given mark
   */
  @Transactional(readOnly = true)
  Map<String,Long> countMark();
  
  /**
   * Get a number of records in database for given mark
   * @param mark the mark of the car
   * @return the number of records in database for given mark
   */
  @Transactional(readOnly = true)
  long countMark(String mark);
  
  /**
   * Get a number of records in database for given mark and model
   * @param mark the mark of the car
   * @param model the model of the car
   * @return the number of records in database for given mark and model
   */
  @Transactional(readOnly = true)
  long countMarkModel(String mark, String model);
  
  /**
   * Get a number of records in database for given mark and type
   * @param mark the mark of the car
   * @param type the type of the car
   * @return the number of records in database for given mark and type
   */
  @Transactional(readOnly = true)
  long countMarkType(String mark, String type);
  
  /**
   * Get a list of information about the cars for given type
   * @param type the type of the car
   * @return the list of the information about the cars for given type
   */
  @Transactional(readOnly = true)
  List<Car> getType(String type);
  
  /**
   * Get a list of information about the cars for given model
   * @param model the model of the car
   * @return the list of the information about the cars for given model
   */
  @Transactional(readOnly = true)
  List<Car> getModel(String model);
  
  /**
   * Get a list of information about the cars for given mark and type
   * @param mark the mark of the car
   * @param type the type of the car
   * @return the list of the information about the cars for given mark and type
   */
  @Transactional(readOnly = true)
  List<Car> getMarkType(String mark, String type);
  
  /**
   * Get a list of information about the cars for given mark and model
   * @param mark the mark of the car
   * @param model the model of the car
   * @return the list of the information about the cars for given mark and model
   */
  @Transactional(readOnly = true)
  List<Car> getMarkModel(String mark, String model);
  
  /**
   * Change the vehicle registration plate (license plate)
   * @param oldVehicleRegistrationPlate the old vehicle registration plate identifier
   * @param newVehicleRegistrationPlate the new vehicle registration plate identifier
   * @return the information about the car
   */
  @Transactional
  Car changeVehicleRegistrationPlate(String oldVehicleRegistrationPlate, String newVehicleRegistrationPlate);
  
  /**
   * Unregister the car
   * @param vehicleRegistrationPlate the vehicle registration plate identifier
   * @param date the day when car was unregistered
   * @return the information about the car
   */
  @Transactional
  Car unregister(String vehicleRegistrationPlate, Date date);
  
  /**
   * Set additional information about a car
   * @param vehicleRegistrationPlate the vehicle registration plate identifier
   * @param notes the additional information
   * @return the information about the car
   */
  @Transactional
  Car setNotes(String vehicleRegistrationPlate, String notes);
  
  /**
   * Append additional information about a car
   * @param vehicleRegistrationPlate the vehicle registration plate identifier
   * @param notes the additional information
   * @return the information about the car
   */
  @Transactional
  Car appendNotes(String vehicleRegistrationPlate, String notes);
}

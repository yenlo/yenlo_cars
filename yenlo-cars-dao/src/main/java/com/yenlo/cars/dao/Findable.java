package com.yenlo.cars.dao;

import java.io.Serializable;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.yenlo.cars.domain.Identifiable;

/**
 * Interface defining finding the records in database
 * @author Thomas Zielinski, 2016
 *
 * @param <T> record to be found in database with {@link Identifiable} key
 * @param <S> the primary key
 */
public interface Findable<T extends Identifiable<S>, S extends Serializable> {
  /**
   * Find a record using primary key
   * @param id primary key's value
   * @return the found record 
   */
  @Transactional(readOnly = true)
  T find(S id);
  
  /**
   * Find records in database
   * @return list of records in database
   */
  @Transactional(readOnly = true)
	List<T> find();
}

package com.yenlo.cars.domain;

import java.io.Serializable;

/**
 * Identity the entity by the primary key
 * 
 * @author Thomas Zielinski, 2016
 */
public interface Identifiable<S extends Serializable> extends Serializable {
    S getId();
}

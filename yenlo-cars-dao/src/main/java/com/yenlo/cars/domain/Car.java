package com.yenlo.cars.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * User entity.
 * 
 * @author Thomas Zielinski, 2016
 */
@Entity
@Table(name = "car")
@NamedQuery(name = "byVRP", query = "select c from Car c where c.vehicleRegistrationPlate=:vehicleRegistrationPlate")
@JsonInclude(value=Include.NON_NULL)
public class Car implements Identifiable<Integer> {
  private static final long serialVersionUID = -8525890830114812017L;
  private int id;
  private String vrp;
  private String mark;
  private String type;
  private String name;
  private Date registered;
  private Date unregistered;
  private Integer bpm;
  private Integer mass;
  private Integer seats;
  private Integer cylinders;
  private Integer capacity;
  private String notes;

  public Car() {
  }

  public Car(String vehicleRegistrationPlate, String mark, String type, String name, Date registered, Integer bpm, Integer seats, Integer capacity, Integer mass, Integer cylinders) {
    this.vrp = vehicleRegistrationPlate;
    this.mark = mark;
    this.type = type;
    this.name = name;
    this.registered = registered;
    this.bpm = bpm;
    this.seats = seats;
    this.capacity = capacity;
    this.cylinders = cylinders;
  }

  @Override
  @Id
  @Column(name = "ID", updatable = false)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Integer getId() {
    return id;
  }

  protected void setId(Integer id) {
    this.id = id;
  }
  
  @Column(name = "VRP")
  public String getVehicleRegistrationPlate() {
    return vrp;
  }

  public void setVehicleRegistrationPlate(String vehicleRegistrationPlate) {
    this.vrp = vehicleRegistrationPlate;
  }

  @Column(name = "MARK", nullable = false, length = 50)
  public String getMark() {
    return mark;
  }

  protected void setMark(String mark) {
    this.mark = mark;
  }

  @Column(name = "TYPE", nullable = false, length = 50)
  public String getModel() {
    return type;
  }

  protected void setModel(String type) {
    this.type = type;
  }

  @Column(name = "NAME", nullable = false, length = 100)
  public String getType() {
    return name;
  }

  protected void setType(String name) {
    this.name = name;
  }

  @Column(name = "REGISTERED", nullable = false)
  public Date getRegistered() {
    return registered;
  }

  protected void setRegistered(Date registered) {
    this.registered = registered;
  }
 
  @Column(name = "UNREGISTERED", nullable = true)
  public Date getUnregistered() {
    return unregistered;
  }

  public void setUnregistered(Date unregistered) {
    this.unregistered = unregistered;
  }

  @Column(name = "BPM", nullable = true)
  public Integer getBPM() {
    return bpm;
  }

  protected void setBPM(Integer bpm) {
    this.bpm = bpm;
  }
  
  protected void setBPM(int bpm) {
    this.bpm = bpm > 0 ? Integer.valueOf(bpm) : null;
  }

  @Column(name = "SEATS", nullable = true)
  public Integer getSeats() {
    return seats;
  }

  protected void setSeats(Integer seats) {
    this.seats = seats;
  }
  
  protected void setSeats(int seats) {
    this.seats = seats > 0 ? Integer.valueOf(seats) : null;
  }
  
  @Column(name = "CAPACITY", nullable = true)
  public Integer getCapacity() {
    return capacity;
  }

  protected void setCapacity(Integer capacity) {
    this.capacity = capacity;
  }
  
  protected void setCapacity(int capacity) {
    this.capacity = capacity > 0 ? Integer.valueOf(capacity) : null;
  }
  
  @Column(name = "CYLINDERS", nullable = true)
  public Integer getCylinders() {
    return cylinders;
  }

  protected void setCylinders(Integer cylinders) {
    this.cylinders = cylinders;
  }
  
  protected void setCylinders(int cylinders) {
    this.cylinders = cylinders > 0 ? Integer.valueOf(cylinders) : null;
  }
  
  @Column(name = "MASS", nullable = true)
  public Integer getMass() {
    return mass;
  }

  protected void setMass(Integer mass) {
    this.mass = mass;
  }
  
  protected void setMass(int mass) {
    this.mass = mass > 0 ? Integer.valueOf(mass) : null;
  }
  
  @Column(name = "NOTES", nullable = true, columnDefinition="TEXT")
  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((vrp == null) ? 0 : vrp.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Car other = (Car)obj;
    if (vrp == null) {
      if (other.vrp != null)
        return false;
    }
    else if (!vrp.equals(other.vrp))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "[vehicleRegistrationPlate=" + vrp + ", mark=" + mark + ", model=" + type + ", type=" + name
        + ", registered=" + registered + ", unregistered=" + unregistered + ", bpm=" + bpm + ", mass=" + mass
        + ", seats=" + seats + ", cylinders=" + cylinders + ", capacity=" + capacity + ", notes=" + notes + "]";
  }
}

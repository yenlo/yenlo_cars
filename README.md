![Yenlo](http://mms.businesswire.com/media/20160329005629/en/516023/21/Yenlo_logo_square.jpg)

# Cars
REST based application to demonstrate usage of the [WSO2 API Manager](http://wso2.com/products/api-manager/). The application reads data from database (MySQL). In can be also use to demonstrate [WSO2 Data Services Server](http://wso2.com/products/data-services-server/) or [WSO2 Enterprise Service Bus](http://wso2.com/products/enterprise-service-bus/).

## Modules
An application (Maven project) contains 3 modules:
- import - the module to import data from _CSV_ file to the MySQL database.
- dao - access to database and to the records
- api - defines the REST API to access the data stored in database

## MySQL database
Install needed table definition [schema.sql](yenlo-cars-import/sql/schema.sql).

To upload the data to the empty database run
```sh
yenlo-cars$ java -jar yenlo-cars-import/target/yenlo-cars-import-0.0.1-jar-with-dependencies.jar --input=./yenlo-cars-import/data/Open_Data_RDW.csv --pattern=honda
```
The data can be uploaded from [cars-backup.sql](yenlo-cars-import/sql/cars-backup.sql)
```sh
mysql> source <path-to-file>/cars-backup.sql
```

## Installation
Under the project's home directory (./yenlo-cars) run
```sh
yenlo-cars$ mvn clean install
```

The _war_ file *yenlo-cars-api#v0.0.1.war* can be deployed to WSO2 API Manager, Tomcat, Jetty or other environment.

You can run as standalone application using Webapp Runner
```sh
yenlo-cars$ java -jar yenlo-cars-api/target/dependency/webapp-runner.jar --port 8900 yenlo-cars-api/target/*.war
```

## Access
* Application *yenlo-cars* is placed on *Heroku* server under URL <https://yenlo-cars.herokuapp.com> and it's ready to use. The access to the application is secured by HTTP Basic Authentication:

User name  | Password
----------: | :----------
yenlo-cars | yenlo-cars-demo  

* API definition in simple (JSON) format under URL <https://yenlo-cars.herokuapp.com/v2/api-docs>.
* API definition as Swagger UI under URL <https://yenlo-cars.herokuapp.com/swagger-ui.html>.

## License
[Apache 2 License](http://www.apache.org/licenses/LICENSE-2.0)

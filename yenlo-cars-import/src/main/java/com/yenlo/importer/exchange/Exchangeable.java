package com.yenlo.importer.exchange;

import com.yenlo.importer.error.ImporterException;

public interface Exchangeable {
  void exchange(String fileName, String pattern, String delimiter, Integer batchSize) throws ImporterException;
}

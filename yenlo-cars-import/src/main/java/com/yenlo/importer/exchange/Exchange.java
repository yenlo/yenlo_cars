package com.yenlo.importer.exchange;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.yenlo.importer.error.ImporterException;
import com.yenlo.importer.reader.Readable;
import com.yenlo.importer.reader.csv.Reader;
import com.yenlo.importer.writer.Writeable;
import com.yenlo.importer.writer.mysql.Writer;

public class Exchange implements Exchangeable {
  private final Writeable writer;
  private final Readable reader = new Reader();
  
  public Exchange() throws ImporterException {
    writer = new Writer();
  }
  
  private void init(String... args) {  

    // create the Options
    Options options = new Options();
    options.addOption(Option.builder("i").required().longOpt("input")
        .desc("input file with data to be loaded to database (CSV format)").hasArg().optionalArg(false).build());
    options.addOption(Option.builder("p").longOpt("pattern")
        .desc("the records contains 'pattern' will be inserted to database").hasArg().optionalArg(false).build());
    options.addOption(Option.builder("d").longOpt("delimiter").desc("fields delimiter used in file").hasArg()
        .optionalArg(false).type(Character.class).build());
    options.addOption(Option.builder("b").longOpt("batch").desc("size of batch").hasArg()
        .optionalArg(false).type(Integer.class).build());
    options.addOption(Option.builder("h").longOpt("help").desc("print this usage").hasArg(false).build());       

    try {
      CommandLine line = new DefaultParser().parse(options, args);
      Integer batchSize = null;
      try {
        batchSize = Integer.parseInt(line.getOptionValue("batch"));
      }
      catch(Exception ex) {
        batchSize = null;
      }
      exchange(line.getOptionValue("input"), 
          line.getOptionValue("pattern", null), 
          line.getOptionValue("delimiter"),
          batchSize);
    }
    catch (Exception ex) {
      help(options, "\nError:\n" + ex.toString());
    }
  }

  private void help(Options options, String footer) {
    HelpFormatter helpFormatter = new HelpFormatter();
    helpFormatter.setWidth(100);
    helpFormatter.printHelp(Exchange.class.getSimpleName().toLowerCase(), null, options, footer, true);
  }

  @Override
  public void exchange(String fileName, String pattern, String delimiter, Integer batchSize) throws ImporterException {
    writer.write(
        reader.read(fileName, pattern, 
            delimiter == null || delimiter.trim().isEmpty() ? ',' : delimiter.charAt(0)),
        batchSize);
  }

  public static void main(String[] args) throws ImporterException {
    new Exchange().init(args);
  }
}

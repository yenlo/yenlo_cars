package com.yenlo.importer.reader;

import java.util.List;

public interface Readable {
  List<String[]> read(String fileName, String pattern, char delimiter);
}

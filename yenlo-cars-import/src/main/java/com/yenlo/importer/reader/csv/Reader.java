package com.yenlo.importer.reader.csv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Reader implements com.yenlo.importer.reader.Readable {
  private final static Log LOGGER = LogFactory.getLog(Reader.class);
  private final static String NOT_REGISTERED = "(?i).*Niet geregistreerd.*";
  
  @Override
  public List<String[]> read(String fileName, String pattern, char delimiter) {
    List<String[]> result = Arrays.<String[]>asList();
    if (fileName != null) {
      try (Stream<String> lines = Files.lines(Paths.get(fileName))) {
        if (pattern != null && !pattern.trim().isEmpty()) {
          result = lines
              .filter(line -> line.matches("(?i).*" + pattern + ".*"))
              .filter(line -> !line.matches(NOT_REGISTERED))
              .map(line -> line.split("\\,"))
              .collect(Collectors.toList());
        }
        else {
          result = lines.filter(line -> !line.matches(NOT_REGISTERED))
              .map(line -> line.split("\\,"))
              .collect(Collectors.toList());
        }
      }
      catch (IOException ignored) {
      }
    }
    LOGGER.info(pattern != null && !pattern.trim().isEmpty() ?
        String.format("Read %d lines (file: '%s', pattern: '(?i).*%s.*')", result.size(), fileName, pattern) :
        String.format("Read %d lines (file: '%s')", result.size(), fileName));
    return result;
  }
}

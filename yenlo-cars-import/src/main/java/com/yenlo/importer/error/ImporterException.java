package com.yenlo.importer.error;

public class ImporterException extends Exception {
  private static final long serialVersionUID = -7477789776054799140L;

  public ImporterException(String message) {
    super(message);
  }
  
  public ImporterException(String message, Throwable cause) {
    super(message, cause);
  }
  
  public ImporterException(Throwable cause) {
    super(cause);
  }
}

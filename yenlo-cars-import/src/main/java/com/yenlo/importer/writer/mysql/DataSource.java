package com.yenlo.importer.writer.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.FileBasedConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;

import com.yenlo.importer.error.ImporterException;

public class DataSource {
  private final static String URL = "jdbc:mysql://localhost/cars";
  private final static String DRIVER = "com.mysql.jdbc.Driver";
  private final static String USERNAME = "root";
  private final static String PASSWORD = "root";
  private final Connection connection;
  private final int batchSize;
  
  public DataSource() throws ImporterException {
    Parameters params = new Parameters();
    FileBasedConfigurationBuilder<FileBasedConfiguration> builder = 
        new FileBasedConfigurationBuilder<FileBasedConfiguration>(
        PropertiesConfiguration.class).configure(params.properties().setFileName("mysql.properties"));
    try {
      Configuration config = builder.getConfiguration();
      Class.forName(config.getString("database.driver", DRIVER));
      connection = DriverManager.getConnection(
          config.getString("database.url", URL), 
          config.getString("database.username", USERNAME), 
          config.getString("database.password", PASSWORD));
      connection.setAutoCommit(false);// Disables auto-commit.
      this.batchSize = config.getInt("database.batch.size", 250);
    }
    catch (ConfigurationException | ClassNotFoundException | SQLException  ex) {
      throw new ImporterException(ex);
    }
  }

  public Connection getConnection() {
    return connection;
  }
  
  public int getBatchSize() {
    return batchSize;
  }
  
  public void close() throws ImporterException {
    try {
      connection.close();
    }
    catch (SQLException ex) {
      throw new ImporterException(ex);
    }
  }
}

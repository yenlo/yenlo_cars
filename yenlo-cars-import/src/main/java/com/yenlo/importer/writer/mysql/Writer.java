package com.yenlo.importer.writer.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.yenlo.importer.error.ImporterException;
import com.yenlo.importer.writer.Writeable;

public class Writer implements Writeable {
  private final static Log LOGGER = LogFactory.getLog(Writer.class);
  private final DataSource dataSource;
  private final static String SQL = "INSERT INTO CAR("
      + "vrp, mark, type, bpm, name, seats, cylinders, capacity, mass, registered) "
      + "VALUES (?,?,?,?,?,?,?,?,?,?)";
  
  public Writer() throws ImporterException {
    dataSource = new DataSource();
  }

  @Override
  public void write(List<String[]> records, Integer batchSize) throws ImporterException {
    PreparedStatement statement = null;
    try {
      statement = dataSource != null && dataSource.getConnection() != null
          ? dataSource.getConnection().prepareStatement(SQL) : null;
    }
    catch (SQLException ex) {
      throw new ImporterException(ex);
    }
    if (statement != null) {
      int loaded = 0;
      int inserted = 0;
      int unparsed = 0;
      for (String[] record : records) {
        if (record != null && record.length == 11) {
          try {
            statement.setString(1, record[0]);
            statement.setString(2, record[1]);
            statement.setString(3, record[2]);
            setInteger(statement, 4, toValue(record[3], null));
            statement.setString(5, record[4]);
            statement.setInt(6, toValue(record[5], 0));
            setInteger(statement, 7, toValue(record[6], null));
            setInteger(statement, 8, toValue(record[7], null));
            setInteger(statement, 9, toValue(record[8], null));
            statement.setDate(10, new java.sql.Date(new SimpleDateFormat("dd/MM/yyyy")
                .parse(record[10] != null && !record[10].trim().isEmpty() ? record[10] : record[9]).getTime()));
            statement.addBatch();
            loaded++;
            if (loaded % (batchSize == null ? dataSource.getBatchSize() : batchSize) == 0) {
              inserted += inserted(statement.executeBatch());
            }
          }
          catch (SQLException | ParseException ex) {
            unparsed++;
            LOGGER.error(ex);
          }
        }
      }
      try {
        inserted += inserted(statement.executeBatch());
        dataSource.getConnection().commit();
        statement.close();
        dataSource.close();
       
      }
      catch (Exception ex) {
        LOGGER.error(ex);
      }
      if (unparsed > 0) {
        LOGGER.info(String.format("Stored %d/%d. Unparsed %d", inserted, records.size(), unparsed));
      }
      else {
        LOGGER.info(String.format("Stored %d/%d", inserted, records.size()));
      }
    }
  }

  private Integer toValue(String value, Integer defaultValue) {
    try {
      return Integer.valueOf(value);
    }
    catch (Exception ex) {
      return defaultValue;
    }
  }
  
  private void setInteger(PreparedStatement statement, int pos, Integer value) throws SQLException {
    if (value == null) {
      statement.setNull(pos, Types.INTEGER);
    }
    else {
      statement.setInt(pos, value.intValue());
    }
  }
  
  private int inserted(int[] array) {
    int inserted = 0;
    for (int i : array) {                
      inserted += i >= 0 || i == Statement.SUCCESS_NO_INFO ? 1 : 0;
    }
    return inserted;

  }
}

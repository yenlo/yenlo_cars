package com.yenlo.importer.writer;

import java.util.List;

import com.yenlo.importer.error.ImporterException;

public interface Writeable {
  void write(List<String[]> records, Integer batchSize) throws ImporterException;
}

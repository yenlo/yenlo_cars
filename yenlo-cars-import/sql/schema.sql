create database cars;
create user cars@localhost identified by 'yenlo-cars';
grant all on cars.* to cars@localhost with grant option;
flush privileges;
use cars;
create table if not exists car(
	id int not null auto_increment,
	vrp varchar(10) not null default 'XX-11111',
	mark varchar(50) not null,
	type varchar(50) not null,
	name varchar(100) not null,
	registered date not null,
	unregistered date null,
	bpm integer,
	mass integer,
	seats integer not null default 0,	
	cylinders integer,
	capacity integer,
	notes text,
	PRIMARY KEY(ID),
	UNIQUE INDEX uidx_vrp(vrp), 
	INDEX idx_mark(mark),
	INDEX idx_type(type),
	INDEX idx_name(name),
	INDEX idx_markname(mark,name),
	INDEX idx_marktype(mark,type),
	INDEX idx_registered(registered),
	INDEX idx_unregistered(unregistered)
);

package com.yenlo.cars.api.request;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Unregister {
  private String date;

  public Unregister() {
  }

  public String getDate() {
    return this.date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Date toDate() {
    Date formatted = null;
    if (date != null && !date.trim().isEmpty()) {
      switch (date.length()) {
        case 6:
          formatted = format(date, "yyMMdd");
          break;
        case 8:
          formatted = format(date, "yyyyMMdd");
          if (formatted == null) {
            formatted = format(date, "yy-MM-dd");
            if (formatted == null) {
              formatted = format(date, "yy/MM/dd");
            }
          }
        default:
          formatted = format(date, "yyyy-MM-dd");
          if (formatted == null) {
            formatted = format(date, "yyyy/MM/dd");
          }
          break;
      }
    }
    return formatted;
  }

  @Override
  public String toString() {
    return "[date=" + this.date + "]";
  }

  private Date format(String source, String pattern) {
    try {
      return new SimpleDateFormat(pattern).parse(source);
    }
    catch (ParseException ignored) {
      return null;
    }
  }
}

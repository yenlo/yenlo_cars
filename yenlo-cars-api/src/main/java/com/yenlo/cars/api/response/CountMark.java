package com.yenlo.cars.api.response;

public class CountMark extends Count {
  private String mark;
  
  public CountMark(long count, String mark) {
    super(count);
    setMark(mark);
  }
  
  public String getMark() {
    return this.mark;
  }
  
  protected void setMark(String mark) {
    this.mark = mark;
  }

  @Override
  public String toString() {
    return "[" + mark + "=" + getCount() + "]";
  } 
}

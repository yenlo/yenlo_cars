package com.yenlo.cars.api.response;

public class Count {
  private long count;
  
  public Count(long count) {
    setCount(count);
  }
  
  public long getCount() {
    return this.count;
  }
  
  protected void setCount(long count) {
    this.count = count;
  }

  @Override
  public String toString() {
    return "[count=" + count + "]";
  } 
}

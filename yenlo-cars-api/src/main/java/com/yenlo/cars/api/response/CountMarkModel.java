package com.yenlo.cars.api.response;

public class CountMarkModel extends CountMark {
  private String model;
  
  public CountMarkModel(long count, String mark, String model) {
    super(count, mark);
    setModel(model);
  }
  
  public String getModel() {
    return this.model;
  }
  
  protected void setModel(String model) {
    this.model = model;
  }

  @Override
  public String toString() {
    return "[" + getMark() + " " + model + " = " + getCount() + "]";
  } 
}

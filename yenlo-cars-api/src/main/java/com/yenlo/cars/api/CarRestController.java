package com.yenlo.cars.api;

import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.yenlo.cars.api.request.Notes;
import com.yenlo.cars.api.request.Unregister;
import com.yenlo.cars.api.response.Count;
import com.yenlo.cars.api.response.CountMark;
import com.yenlo.cars.api.response.CountMarkModel;
import com.yenlo.cars.api.response.CountMarkType;
import com.yenlo.cars.dao.Cars;
import com.yenlo.cars.domain.Car;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@EnableWebMvc
public class CarRestController {
  @Autowired
  Cars cars;
  
  private final static Log LOG = LogFactory.getLog(CarRestController.class);
 
  @RequestMapping("/")
  public String welcome() {
    return "Welcome to 'Cars REST API'.";
  }

  @RequestMapping(path = "/count",  method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="# of cars records in database", response=Long.class, notes="Returns the number of records in database.")
  @ApiResponse(code=200, response=Count.class, message="OK")
  @ResponseBody
  public ResponseEntity<Count> count()  {
    logRequest(getCurrentMethod());    
    ResponseEntity<Count> response = new ResponseEntity<>(new Count(cars.count()), HttpStatus.OK);
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/all", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars", response=Car.class, responseContainer="List", notes="Returns the information about the cars.")
  @ApiResponses({
      @ApiResponse(code=200, response=Car.class, responseContainer="List", message="OK"),
      @ApiResponse(code=204, message="No content")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> find() {
    logRequest(getCurrentMethod());
    ResponseEntity<List<Car>> response = response(cars.find());
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}",
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the car", response=Car.class, notes="Returns the information about the car.")
  @ApiResponses({
    @ApiResponse(code=200, response=Car.class, message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<Car> find(@PathVariable String vehicleRegistrationPlate) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlate);
    ResponseEntity<Car> response = isEmpty(vehicleRegistrationPlate) ? new ResponseEntity<>(HttpStatus.BAD_REQUEST) : response(cars.find(vehicleRegistrationPlate));
    logResponse(response);
    return response;
  }
  
  @RequestMapping(path = "/count/marks", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="# of cars records in database for the every mark", response=CountMark.class, notes="Returns the number of records in database for the every mark.")
  @ApiResponse(code=200, response=List.class, message="OK")
  @ResponseBody
  public ResponseEntity<List<CountMark>> countMark() {
    logRequest(getCurrentMethod());
    Map<String,Long> result = cars.countMark();
    ResponseEntity<List<CountMark>> response = null;
    List<CountMark> list = new ArrayList<>();
    if (result != null && !result.isEmpty()) {
      for(Entry<String, Long> entry : result.entrySet()) {
        list.add(new CountMark(entry.getValue(), entry.getKey()));
      }
    }
    response = new ResponseEntity<>(list, HttpStatus.OK);
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/mark/{mark}", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars for given mark", response=Car.class, responseContainer="List", notes="Returns the information about the car for given mark.")
  @ApiResponses({
    @ApiResponse(code=200, response=Car.class, responseContainer="List", message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> getMark(
      @ApiParam(name="mark", value="The car's mark", required=true, example="MERCEDES-BENZ")
      @PathVariable String mark) {
    logRequest(getCurrentMethod(), mark);
    ResponseEntity<List<Car>> response = isEmpty(mark) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : response(cars.getMark(mark));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/count/mark/{mark}", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="# of the cars for given mark", response=CountMark.class, notes="Returns the number of records for given mark.")
  @ApiResponses({
    @ApiResponse(code=200, response=CountMark.class, message="OK"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<CountMark> countMark(
      @ApiParam(name="mark", value="The car's mark", required=true, example="MERCEDES-BENZ")
      @PathVariable String mark) {
    logRequest(getCurrentMethod(), mark);
    ResponseEntity<CountMark> response = isEmpty(mark) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          new ResponseEntity<>(new CountMark(cars.countMark(mark), mark), HttpStatus.OK);
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/model/{model}", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars for given model", response=Car.class, responseContainer="List", notes="Returns the information about the cars for given model.")
  @ApiResponses({
    @ApiResponse(code=200, response=Car.class, responseContainer="List", message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> getModel(
      @ApiParam(name="model", value="The car's model", required=true, example="E200")
      @PathVariable String model) {
    logRequest(getCurrentMethod(), model);
    ResponseEntity<List<Car>> response = isEmpty(model) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : response(cars.getType(model));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/type/{type}", 
      method = RequestMethod.GET,
      consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars for given type", response=Car.class, responseContainer="List", notes="Returns the information about the cars for given type.")
  @ApiResponses({
    @ApiResponse(code=200, response=Car.class, responseContainer="List", message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> getType(
      @ApiParam(name="type", value="The car's type", required=true, example="cabriolet")
      @PathVariable String type) {
    logRequest(getCurrentMethod(), type);
    ResponseEntity<List<Car>> response = isEmpty(type) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : response(cars.getModel(type));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/mark/{mark}/model/{model}", 
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars for given mark and model", response=Car.class, responseContainer="List", notes="Returns the information about the cars for given mark and model.")
  @ApiResponses({
    @ApiResponse(code=200, response=Car.class, responseContainer="List", message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> getMarkModel(
      @ApiParam(name="mark", value="The car's mark", required=true, example="HONDA")
      @PathVariable String mark,
      @ApiParam(name="model", value="The car's model", required=true, example="CR-V")
      @PathVariable String model) {
    logRequest(getCurrentMethod(), mark, model);
    ResponseEntity<List<Car>> response = isEmpty(mark) || isEmpty(model) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          response(cars.getMarkType(mark, model));
    logResponse(response);
    return response;
  }
  
  @RequestMapping(path = "/count/mark/{mark}/model/{model}", 
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="# of the cars for given mark and model", response=Long.class, notes="Returns the number of records for given mark and model.")
  @ApiResponses({
    @ApiResponse(code=200, response=Long.class, message="OK"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<CountMarkModel> countMarkModel(
      @ApiParam(name="mark", value="The car's mark", required=true, example="HONDA")
      @PathVariable String mark,
      @ApiParam(name="model", value="The car's model", required=true, example="CR-V")
      @PathVariable String model) {
    logRequest(getCurrentMethod(), mark, model);
    ResponseEntity<CountMarkModel> response = isEmpty(mark) || isEmpty(model) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          new ResponseEntity<>(new CountMarkModel(cars.countMarkModel(mark, model), mark, model), HttpStatus.OK);
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/mark/{mark}/type/{type}", 
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="information about the cars for given mark and type", response=List.class, notes="Returns the information about the cars for given mark and type.")
  @ApiResponses({
    @ApiResponse(code=200, response=List.class, message="OK"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<List<Car>> getMarkType(
      @ApiParam(name="mark", value="The car's mark", required=true, example="HONDA")
      @PathVariable String mark, 
      @ApiParam(name="type", value="The car's type", required=true, example="sedan")
      @PathVariable String type) {
    logRequest(getCurrentMethod(), mark, type);
    ResponseEntity<List<Car>> response = isEmpty(mark) || isEmpty(type) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          response(cars.getMarkModel(mark, type));
    logResponse(response);
    return response;
  }
  
  @RequestMapping(path = "/count/mark/{mark}/type/{type}", 
      method = RequestMethod.GET,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="# of the cars for given mark and type", response=Long.class, notes="Returns the number of records for given mark and type.")
  @ApiResponses({
    @ApiResponse(code=200, response=Long.class, message="OK"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<CountMarkType> countMarkType(
      @ApiParam(name="mark", value="The car's mark", required=true, example="HONDA")
      @PathVariable String mark,
      @ApiParam(name="type", value="The car's type", required=true, example="sdan")
      @PathVariable String type) {
    logRequest(getCurrentMethod(), mark, type);
    ResponseEntity<CountMarkType> response = isEmpty(mark) || isEmpty(type) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          new ResponseEntity<>(new CountMarkType(cars.countMarkType(mark, type), mark, type), HttpStatus.OK);
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}/unregister", 
      method = RequestMethod.PUT,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="unregister the car", response=Car.class, notes="Returns the information about the car after an unregistration.")
  @ApiResponses({
    @ApiResponse(code=202, response=Car.class, message="Accepted"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing"),
    @ApiResponse(code=406, message="The date format invalid")
  })
  @ResponseBody
  public ResponseEntity<Car> unregister(
      @ApiParam(name="vehicleRegistrationPlate", value="The vehicle registration plate", required=true, example="DW417LK")
      @PathVariable String vehicleRegistrationPlate,
      @ApiParam(name="unregister", value="The date when car was unregistered", example="{\"date\":\"20160110\"}")
      @RequestBody Unregister unregister) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlate);
    ResponseEntity<Car> response = null;
    if (unregister == null) {
      response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    else {
      Date date = unregister.toDate();
      if (date == null) {
        response = new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
      }
      else {
        response = update(cars.unregister(vehicleRegistrationPlate, date));
      }
    }
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}/notes", 
      method = RequestMethod.PUT,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="set an additional information about a car", response=Car.class, notes="Returns the information about the car after set an additional information.")
  @ApiResponses({
    @ApiResponse(code=202, response=Car.class, message="Accepted"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<Car> setNotes(
      @ApiParam(name="vehicleRegistrationPlate", value="The vehicle registration plate", required=true, example="DW417LK")
      @PathVariable String vehicleRegistrationPlate,
      @ApiParam(name="notes", value="Set an additional informations about the car", required=true, example="{\"notes\":\"Some additional info\"}")
      @RequestBody Notes notes) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlate);
    ResponseEntity<Car> response = isEmpty(vehicleRegistrationPlate) || notes == null || isEmpty(notes.getNotes()) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
          update(cars.setNotes(vehicleRegistrationPlate, notes.getNotes()));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}/notes/clear", 
      method = RequestMethod.PUT,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="clear an additional information about a car", response=Car.class, notes="Returns the information about the car after removing an additional information.")
  @ApiResponses({
    @ApiResponse(code=202, response=Car.class, message="Accepted"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<Car> setNotes(
      @ApiParam(name="vehicleRegistrationPlate", value="The vehicle registration plate", required=true, example="DW417LK")
      @PathVariable String vehicleRegistrationPlate) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlate);
    ResponseEntity<Car> response = isEmpty(vehicleRegistrationPlate) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : update(cars.setNotes(vehicleRegistrationPlate, null));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}/notes/append", 
      method = RequestMethod.PUT,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="append an additional information about a car", response=Car.class, notes="Returns the information about the car after appending an additional information.")
  @ApiResponses({
    @ApiResponse(code=202, response=Car.class, message="Accepted"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<Car> appendNotes(
      @ApiParam(name="vehicleRegistrationPlate", value="The vehicle registration plate", required=true, example="DW417LK")
      @PathVariable String vehicleRegistrationPlated, 
      @ApiParam(name="notes", value="Append an additional informations about the car", required=true, example="{\"notes\":\"Appended additional info\"}")
      @RequestBody Notes notes) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlated);
    ResponseEntity<Car> response = isEmpty(vehicleRegistrationPlated) || notes == null || isEmpty(notes.getNotes()) ? 
        new ResponseEntity<>(HttpStatus.BAD_REQUEST) : update(cars.appendNotes(vehicleRegistrationPlated, notes.getNotes()));
    logResponse(response);
    return response;
  }

  @RequestMapping(path = "/vrp/{vehicleRegistrationPlate}/to", 
      method = RequestMethod.PUT,
      produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
  @ApiOperation(value="change the vehicle registration plate", response=List.class, notes="Returns the information about the car after a change the vehicle registration plate.")
  @ApiResponses({
    @ApiResponse(code=202, response=Car.class, message="Accepted"),
    @ApiResponse(code=204, message="No content"),
    @ApiResponse(code=400, message="Parameter is missing")
  })
  @ResponseBody
  public ResponseEntity<Car> changeVehicleRegistrationPlated(
      @ApiParam(name="vehicleRegistrationPlate", value="The vehicle registration plate", required=true, example="DW417LK")
      @PathVariable String vehicleRegistrationPlate, 
      @ApiParam(name="newVehicleRegistrationPlate", value="The new vehicle registration plate", required=true, example="DW999ZA")
      @RequestBody String newVehicleRegistrationPlate) {
    logRequest(getCurrentMethod(), vehicleRegistrationPlate);
    ResponseEntity<Car> response = isEmpty(vehicleRegistrationPlate) || 
        isEmpty(newVehicleRegistrationPlate) || 
        vehicleRegistrationPlate.trim().equalsIgnoreCase(newVehicleRegistrationPlate.trim()) ? 
            new ResponseEntity<>(HttpStatus.BAD_REQUEST) : 
              update(cars.changeVehicleRegistrationPlate(vehicleRegistrationPlate, newVehicleRegistrationPlate));
    logResponse(response);
    return response;
  }

  private ResponseEntity<Car> response(Car car) {
    return car == null ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(car, HttpStatus.OK);
  }
  
  private ResponseEntity<Car> update(Car car) {
    return car == null ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(car, HttpStatus.ACCEPTED);
  }

  private ResponseEntity<List<Car>> response(List<Car> cars) {
    return cars == null || cars.isEmpty() ? new ResponseEntity<List<Car>>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<List<Car>>(cars, HttpStatus.OK);
  }

  private boolean isEmpty(String text) {
    return text == null || text.trim().isEmpty();
  }
  
  private Method getCurrentMethod() {
    String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
    for(Method method : getClass().getMethods()){
        if(method.getName().equals(methodName)){
            return method;
        }
    }
    return null;
  }
  
  private RequestMapping getAnnotation(Method method) {
    return method != null ? method.getAnnotation(RequestMapping.class) : null;
  }
  
  private void logRequest(Method method, String... value) {
    RequestMapping rq = getAnnotation(method);
    String path = null;
    if (rq.path() != null && rq.path().length == 1 && value != null && value.length > 0) {
      path = rq.path()[0];
      for (String val : value) {
        path = path.replaceFirst("\\{.*\\}", val);
      }
    }
    LOG.info("Request: [path: " + (!isEmpty(path) ? "[" + path + "]" : Arrays.toString(rq.path())) + 
        ", method: " + Arrays.toString(rq.method()) + 
        ", consumes: " + Arrays.toString(rq.consumes()) + 
        ", produces: " + Arrays.toString(rq.produces()));
  }
  
  private void logResponse(ResponseEntity<?> response) {
    LOG.info("Response:[statusCode: " + response.getStatusCode() + ",  has body: " + response.hasBody()
        + ", body length: " + new DecimalFormat("#,##0").format(response.getBody().toString().length()) + "]");
  }
}

package com.yenlo.cars.api.request;

public class Notes {
  private String notes;

  public Notes() {
  }

  public String getNotes() {
    return this.notes;
  }

  public void setNote(String notes) {
    this.notes = notes;
  }

  @Override
  public String toString() {
    return "[notes=" + this.notes + "]";
  }
}

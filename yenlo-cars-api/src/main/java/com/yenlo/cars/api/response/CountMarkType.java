package com.yenlo.cars.api.response;

public class CountMarkType extends CountMark {
private String type;
  
  public CountMarkType(long count, String mark, String type) {
    super(count, mark);
    setType(type);
  }
  
  public String getType() {
    return this.type;
  }
  
  protected void setType(String type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "[" + getMark() + " " + type + " = " + getCount() + "]";
  } 
}
